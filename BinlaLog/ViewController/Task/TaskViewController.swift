//
//  TaskViewController.swift
//  BinlaLog
//
//  Created by Tanakorn Rattanajariya on 1/4/2562 BE.
//  Copyright © 2562 Tanakorn. All rights reserved.
//

import UIKit

class TaskViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var tf_title: UITextField!
    @IBOutlet weak var tf_date: UITextField!
    @IBOutlet weak var tf_rotation: UITextField!
    @IBAction func btn_cancel_action(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btn_done_action(_ sender: UIBarButtonItem) {
        APIRotation.updateTask(viewModel: self.viewModel, finish: {(success) in
            self.dismiss(animated: true, completion: nil)
        }, fail: {(error) in
            
        })
    }
    @IBAction func tf_title_change(_ sender: UITextField) {
        self.viewModel.title = sender.text ?? ""
    }
    var note : Note?
    var viewModel = ViewModel(){
        didSet{
            self.tf_rotation.watch(subject: self.viewModel.rotation)
            self.tf_date.watch(subject: self.viewModel.date.convertToStringOnlyDate())
            self.tf_title.watch(subject: self.viewModel.title)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initRotation()
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        NotificationCenter.default.addObserver(self, selector: #selector(pickRotation), name: .pickerChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pickDate), name: .dateChange, object: nil)
        self.initViewModel()
    }
    func initViewModel(){
        if self.note != nil{
            self.viewModel.date = self.note!.date
            self.viewModel.rotation = BackRotation.getInstance().get(id: self.note!.rotationId)?.rotationname ?? ""
            self.viewModel.title = self.note!.title
            self.viewModel.id = self.note!.id
        }
    }
    @objc func pickRotation(notification:Notification) {
        if let rotationName = notification.userInfo?["data"] as? String{
            self.viewModel.rotation = rotationName
        }
    }
    @objc func pickDate(notification:Notification){
        if let date = notification.userInfo?["date"] as? Date{
            self.viewModel.date = date
        }
    }
    func initRotation(){
        self.viewModel.rotations = Array(BackRotation.getInstance().listMyRotation())
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            Helper.showPicker(sender: self, arr: self.viewModel.rotations.map({
                return $0.rotationname
            }))
            return false
        }else if textField.tag == 3{
            Helper.showDatePicker(sender: self, date: Date(), restrictDate: nil)
            return false
        } else{
            return true
        }
        
    }
    
}
extension TaskViewController{
    struct ViewModel {
        var rotations = [Rotation]()
        var rotation : String = ""
        var date = Date()
        var title : String = ""
        var id : String?
    }
}
