//
//  LogTask.swift
//  BinlaLog
//
//  Created by Tanakorn Rattanajariya on 16/5/2562 BE.
//  Copyright © 2562 Tanakorn. All rights reserved.
//

import Foundation
import RealmSwift
class LogTask:Object{
    @objc dynamic var id : String = ""
    @objc dynamic var updatetime : Date = Date()
    @objc dynamic var lbUserId : String = ""
    @objc dynamic var latitude : Double = 0.0
    @objc dynamic var longitude : Double  = 0.0
    @objc dynamic var logQuestId : String = ""
    @objc dynamic var taskId : String = ""
}
