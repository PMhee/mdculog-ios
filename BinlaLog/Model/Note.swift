//
//  Note.swift
//  BinlaLog
//
//  Created by Tanakorn Rattanajariya on 2/4/2562 BE.
//  Copyright © 2562 Tanakorn. All rights reserved.
//

import Foundation
import RealmSwift
class Note:Object{
    @objc dynamic var id : String = ""
    override static func primaryKey() -> String? {
        return "id"
    }
    @objc dynamic var title : String = ""
    @objc dynamic var rotationId : String = ""
    @objc dynamic var date : Date = Date()
}
